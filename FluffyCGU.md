# Fluffy - Balade à Paris s’engage à :  #


### Utilisation ###

Votre identifiant Android sera utilisé uniquement à des fins de statistiques relatives aux utilisateurs.

### Association à des informations personnelles ou à d'autres identifiants  ###

Votre identifiant ne permet pas de vous associer à des informations personnelles identifiables ni à un identifiant permanent de votre l'appareil (par exemple, SSAID, adresse MAC, code IMEI, etc.).

### Respect des choix de l'utilisateur ###

En cas de réinitialisation de votre identifiant, celui-ci ne sera pas associé à l'identifiant précédent ni à des données dérivées de ce dernier.

### Respect des conditions d'utilisation ###

Votre identifiant publicitaire sera utilisé que conformément aux présentes conditions, y compris par les tiers auxquels nous le communiquons dans le cadre de notre activité (tiers d’analyses statistiques uniquement).